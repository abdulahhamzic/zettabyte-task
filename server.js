const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const dotenv = require('dotenv');
const User = require('./classes/User');
const api = require('./api');

const app = express();
dotenv.config({});
mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
User.configPassport(passport);

app.use(session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

api(app);

const port = process.env.PORT || 3000;
app.listen(port, (err) => {
    if (err) throw err;
    console.log('Listening on port ' + port);
});
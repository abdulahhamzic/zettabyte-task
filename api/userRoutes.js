const passport = require('passport');
const User = require('../classes/User');

module.exports = app => {
    app.post('/api/register', passport.authenticate('local-signup'), (req, res) => {
        res.send({ userData: req.user });
    });

    app.post('/api/login', passport.authenticate('local-login'), (req, res) => {
        res.send({ userData: req.user });
    });

    app.get('/api/logout', (req, res) => {
        req.logout();
        res.send({ userData: false });
    });

    app.delete('/api/deleteUser', (req, res) => {
        const user = new User(req.body);
        user.deleteUser()
            .then(result => res.send(result))
            .catch(err => res.send(err));
    });

    app.put('/api/updateUser', (req, res) => {
        const user = new User(req.body);
        user.updateUser()
            .then(result => res.send(result))
            .catch(err => res.send(err));
    });
}
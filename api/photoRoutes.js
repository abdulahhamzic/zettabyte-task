const Photo = require('../classes/Photo');

module.exports = app => {
    app.post('/api/addPhoto', (req, res) => {
        const photo = new Photo(req.body);
        photo.addPhoto()
            .then(result => res.send(result))
            .catch(err => res.send(err));
    });

    app.get('/api/photo/:id', (req, res) => {
        const photo = new Photo({ _id: req.params.id });
        photo.getPhoto()
            .then(result => res.send(result))
            .catch(err => res.send(err));
    });

    app.delete('/api/photo/:id', (req, res) => {
        const photo = new Photo({ _id: req.params.id });
        photo.deletePhoto()
            .then(result => res.send(result))
            .catch(err => res.send(err));
    });

    app.put('/api/photo/:id', (req, res) => {
        const photo = new Photo({ _id: req.params.id, ...req.body });
        photo.updatePhoto()
            .then(result => res.send(result))
            .catch(err => res.send(err));
    });
}
const userRoutes = require('./userRoutes');
const photoRoutes = require('./photoRoutes');

module.exports = app => {
    userRoutes(app);
    photoRoutes(app);
}
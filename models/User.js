const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const userSchema = new mongoose.Schema({
    dateOfBirth: Date,
    email: String,
    firstName: String,
    gender: String,
    lastName: String,
    location: String,
    password: String,
    username: String,
    photos: [String]
});

userSchema.methods.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('users', userSchema);
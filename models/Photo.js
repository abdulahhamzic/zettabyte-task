const mongoose = require('mongoose');

module.exports = mongoose.model('photos', new mongoose.Schema({
    dateOfCreation: Date,
    name: String,
    type: String,
    user: String
}));
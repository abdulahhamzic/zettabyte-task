const PhotoModel = require('../models/Photo');
const User = require('./User');

module.exports = class {
    constructor(photoData) {
        Object.keys(photoData).forEach(key => this[key] = photoData[key]);
    }

    addPhoto() {
        return new Promise((resolve, reject) => {
            if (!this.isPhotoValid()) return reject('Photo data is not valid');
            const { name, user } = this;
            const photo = new PhotoModel({
                name,
                user,
                dateOfCreation: new Date()
            });
            photo.save((err) => {
                if (err) return reject(err);
                const username = this.user;
                const photoName = this.name;
                const user = new User({ username, photos: [photoName] });
                user.updateUser()
                    .then(() => resolve(photo))
                    .catch(err => resolve(err));
            });
        });
    }

    isPhotoValid() {
        const requiredProps = ['name', 'user'];
        return requiredProps.every(prop => this[prop]);
    }

    deletePhoto() {
        return new Promise((resolve, reject) => {
            this.getPhoto()
                .then(photo => {
                    photo.remove();
                    resolve('Photo deleted');
                })
                .catch(err => reject(err));
        });
    }

    getPhoto() {
        return new Promise((resolve, reject) => {
            if (!this._id) return reject('Photo id not provided');
            const _id = this._id;
            PhotoModel.findOne({ _id }, (err, photo) => {
                if (err) return reject('Mongo threw an error');
                if (!photo) return reject('Photo not found');
                resolve(photo);
            });
        });
    }

    updatePhoto() {
        return new Promise((resolve, reject) => {
            this.getPhoto()
                .then((photo) => {
                    Object.keys(photo._doc).forEach((prop) => {
                        if (this[prop]) photo[prop] = this[prop];
                    });
                    photo.save((err) => {
                        if (err) return reject(err);
                        resolve(photo);
                    });
                })
                .catch(err => reject(err));
        });
    }
}
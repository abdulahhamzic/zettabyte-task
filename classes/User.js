const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const UserModel = require('../models/User');

module.exports = class User {
    constructor(userData) {
        Object.keys(userData).forEach(key => this[key] = userData[key]);
    }

    static configPassport(passport) {
        passport.serializeUser((user, done) => done(null, user.id));
        passport.deserializeUser((id, done) => {
            UserModel.findById(id, (err, user) => done(err, user));
        });
        User.configLocalSignup(passport);
        User.configLocalLogin(passport);
    }

    static configLocalSignup(passport) {
        passport.use('local-signup', new LocalStrategy({
            passReqToCallback: true
        }, (req, username, password, done) => {
            process.nextTick(() => {
                // Checking to see if the user trying to login already exists
                UserModel.findOne({ username }, (err, user) => {
                    if (err) return done(err);
                    if (user) return done(null, false);
                    else {
                        // if there is no user with that username, create the user
                        const newUser = new UserModel({
                            dateOfBirth: req.body.dateOfBirth,
                            email: req.body.email,
                            firstName: req.body.firstName,
                            gender: req.body.gender,
                            lastName: req.body.lastName,
                            location: req.body.location,
                            username
                        });
                        newUser.password = newUser.generateHash(password);
                        newUser.save((err) => {
                            if (err) throw err;
                            return done(null, newUser);
                        });
                    }
                });
            });
        }));
    }

    static configLocalLogin(passport) {
        passport.use('local-login', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        }, (username, password, done) => {
            // Checking to see if the user trying to login already exists
            UserModel.findOne({ username }, (err, user) => {
                if (err) return done(err);
                if (!user) return done(null, false); // if no user is found, return the message
                if (!user.validPassword(password)) return done(null, false); // if the user is found but the password is wrong
                return done(null, user); // all is well, return successful user
            });
        }));
    }

    deleteUser() {
        return new Promise((resolve, reject) => {
            this.getUser()
                .then(user => {
                    user.remove();
                    resolve('User deleted');
                })
                .catch(err => reject(err));
        });
    }

    getUser() {
        return new Promise((resolve, reject) => {
            if (!this.username) return reject('Username not provided');
            const username = this.username;
            UserModel.findOne({ username }, (err, user) => {
                if (err) return reject('Mongo threw an error');
                if (!user) return reject('User not found');
                resolve(user);
            });
        });
    }

    updateUser() {
        return new Promise((resolve, reject) => {
            this.getUser()
                .then(user => {
                    Object.keys(user._doc).forEach((prop) => {
                        if (this[prop]) {
                            if (prop === 'photos') user[photos].push(...this[photos]);
                            else user[prop] = this[prop];
                        };
                    });
                    user.save((err) => {
                        if (err) return reject(err);
                        resolve(user);
                    });
                })
                .catch(err => reject(err));
        });
    }
}